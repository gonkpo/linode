Hampel Linode
=============

A Linode API wrapper using Guzzle

By [Simon Hampel](http://hampelgroup.com/).

Installation
------------

The recommended way of installing Hampel Linode is through [Composer](http://getcomposer.org):

    {
        "require": {
            "hampel/linode": "1.0.*"
        }
    }
    
Usage
-----

    <?php

	use Hampel\Linode\Service\LinodeConfig;
	use Hampel\Linode\Service\LinodeService;
	use Hampel\Linode\Command\Test;

	// long-hand initialisation method
	$config = new LinodeConfig();
	$config->set("api_key");
	$client = new Guzzle\Http\Client();
	$linode = new LinodeService($client, $config);
	$linode->init();
	
	// alternative static builder
	$linode = LinodeService::build("api_key");
		
	// echo test, API echoes back parameters passed
	$test = new Test($linode);
	$response = $test->echoTest(array("foo" => "bar"));
	
	var_dump($response);
	
	// get domain key	
	$user = new User($linode);
	$response = $user->getAPIKey("username", "password");

	var_dump($response);
	
	// alternative static method for getting API key without needing to instantiate LinodeService
	$response = LinodeConfig::requestAPIKey("username", "password");
	
	var_dump($response);
		
	// create a new domain entry
	$domain = new Domain($linode);
	$options = array("ttl_sec" => 3600);
	$response = $domain->create("domain-name.com", "master", "soa_email@domain-name.com", $options);

	var_dump($response);
	
	// retrieve details about a domain
	$domain = new Domain($linode);
	$response = $domain->list(12345); // specify domain id as parameter
	
	
	?>

Notes
-----
 
The following calls have been implemented:

* domain.*
* domain.resource.*
* test.*
* user.*
 
TODO: 

The following calls still need to be implemented:

* linode.*
* linode.config.*
* linode.disk.*
* linode.ip.*
* linode.job.*
* nodebalancer.*
* nodebalancer.config.*
* nodebalancer.node.*
* stackscript.*
* api.*
* account.*
* avail.*

Unit Testing
------------

Rename phpunit.xml.dist to phpunit.xml to set up unit testing, configure your API Key in the php section:

	<php>
		<const name="API_KEY" value="Linode API Key goes here" />
	</php>
	
To run mock tests only and ignore network tests, run: phpunit --exclude-group network
