<?php namespace Hampel\Linode\Command;

use Hampel\Linode\Service\LinodeException;
use Hampel\Linode\Response\DomainData;

/**
 * Domain Linode API group
 *
 */
class Domain extends Family
{
	/** @var string Prefix for commands */
	protected $prefix = 'domain';

	/** @var array allowable parameters to create and update calls */
	protected $parameters = array(
		"domain", // required - zone's name
		"domainid", // required for update
		"description", // default "" - currently undisplayed
		"type", // required must be master or slave
		"soa_email", // required when type=master
		"refresh_sec", // default 0
		"retry_sec", // default 0
		"expire_sec", // default 0
		"ttl_sec", // default 0
		"status", // default 1 - must be 0, 1 or 2 (disabled, active, edit mode)
		"master_ips", // default "" - when type=slave, the zone's master DNS servers list, semicolon separated
		"axfr_ips" // default "" - IP addresses allowed to AXFR the entire zone, semicolon separated
	);

	/**
	 * domain.create
	 *
	 * @param string $domain	domain name to create
	 * @param string $type		must be "master" or "slave"
	 * @param array $options	array of key-value pairs for other optional values (see parameters array)
	 *
	 * @throws LinodeException
	 *
	 * @return number domain identifier
	 */
	public function create($domain, $type = "master", array $options = array())
	{
		$options['domain'] = $domain;
		$options['type'] = $type;

		$command = $this->prefix . '.create';
		$request_headers = array();
		$request_options['query'] = $this->processOptions($options);

		$data = $this->linode->get($command, $request_headers, $request_options);

		if (!array_key_exists('DomainID', $data)) throw new LinodeException("Invalid data returned from {$command} - no DomainID found");

		return $data['DomainID'];
	}

	/**
	 * domain.update
	 *
	 * @param number $domainid	domainid to update
	 * @param array $options	array of key-value pairs for other optional values (see defaults array)
	 *
	 * @throws LinodeException
	 *
	 * @return number domain identifier
	 */
	public function update($domainid, array $options = array())
	{
		$options['domainid'] = $domainid;

		$command = $this->prefix . '.update';
		$request_headers = array();
		$request_options['query'] = $this->processOptions($options);

		$data = $this->linode->get($command, $request_headers, $request_options);

		if (!array_key_exists('DomainID', $data)) throw new LinodeException("Invalid data returned from {$command} - no DomainID found");

		return $data['DomainID'];
	}

	/**
	 * domain.delete
	 *
	 * @param number $domainid	Linode domain ID to delete
	 *
	 * @throws LinodeException
	 *
	 * @return number domainid of the domain deleted
	 */
	public function delete($domainid)
	{
		$command = $this->prefix . '.delete';
		$request_headers = array();
		$request_options['query'] = array("domainid" => intval($domainid));

		$data = $this->linode->get($command, $request_headers, $request_options);

		if (!array_key_exists('DomainID', $data)) throw new LinodeException("Invalid data returned from {$command} - no DomainID found");

		return $data['DomainID'];
	}

	/**
	 * domain.list
	 *
	 * @param number $domainid	optional - if specified, lists details for this domain ID, otherwise lists all available domains
	 *
	 * @return array of DomainData objects or single DomainData object if domainid specified
	 */
	public function listDomain($domainid = 0)
	{
		$command = $this->prefix . '.list';
		$request_headers = array();
		$request_options = array();

		$domainid = intval($domainid);

		if ($domainid > 0)
		{
			$request_options['query'] = array("domainid" => $domainid);
		}

		$data = $this->linode->get($command, $request_headers, $request_options);

		if ($domainid > 0) return DomainData::extractSingleDomain($data, $domainid);
		else return DomainData::extractDomains($data);
	}

	/**
	 * domain.list name
	 *
	 * @param string $domain_name
	 *
	 * @return array single DomainData object
	 */
	public function findDomain($domain_name)
	{
		$command = $this->prefix . '.list';
		$request_headers = array();
		$request_options = array();

		$request_options['query'] = array("domain" => $domain_name);

		$data = $this->linode->get($command, $request_headers, $request_options);

		return DomainData::extractSingleDomain($data, $domain_name);
	}

	public function __call($func, $args)
	{
		switch ($func)
		{
			case 'list':
				return $this->listDomain(isset($args[0]) ? $args[0] : 0);
				break;
			default:
				trigger_error("Call to undefined method ".__CLASS__."::$func()", E_USER_ERROR);
				die ();
		}
	}
}

?>