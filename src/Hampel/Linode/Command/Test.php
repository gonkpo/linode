<?php namespace Hampel\Linode\Command;

/**
 * Test Linode API group
 *
 */
class Test extends Family
{
	/** @var string Prefix for commands */
	protected $prefix = 'test';

	/**
	 * test.echo
	 *
	 * @param array associative array of arbitrary key-value pairs to send as query parameters to be echoed back
	 *
	 * @return array of key-value pairs echoing the parameters sent to the call
	 */
	public function echoTest(array $parameters)
	{
		$command = $this->prefix . '.echo';
		$request_headers = array();
		$request_options = array('query' => $parameters);

		return $this->linode->get($command, $request_headers, $request_options);
	}

	public function __call($func, $args)
	{
		switch ($func)
		{
			case 'echo':
				return $this->echoTest($args);
				break;
			default:
				trigger_error("Call to undefined method ".__CLASS__."::$func()", E_USER_ERROR);
				die ();
		}
	}
}

?>