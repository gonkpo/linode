<?php namespace Hampel\Linode\Command;

use Hampel\Linode\Service\LinodeException;
use Hampel\Linode\Response\DomainResourceData;

/**
 * Domain.Resource Linode API group
 *
 */
class DomainResource extends Family
{
	/** @var string Prefix for commands */
	protected $prefix = 'domain.resource';

	protected $parameters = array(
		"domainid", // required
		"resourceid", // required for update
		"type", // required one of NS, MX, A, AAAA, CNAME, TXT, SRV
		"name", // default "" - hostname of FQDN. When type=MX the subdomain to delegate to the target MX server
		"target", 	// default ""
		// when type=MX the hostname
		// when type=CNAME the target of the alias
		// when type=TXT the value of the record
		// when type=A or AAAA the token of [remote_addr] will be substituted with the IP address of the request
		"priority", // default 10
		"weight", // default 5,
		"port", // default 80,
		"protocol", // default 'udp' - the protocol to append to an SRV record. Ignored on other record types
		"ttl_sec" // default 0 - TTL. Leave as 0 to accept Linode default
	);

	/**
	 * domain.resource.create
	 *
	 * @param string $domainid	domainid to create a resource for
	 * @param string $type		must be one of "NS, MX, A, AAAA, CNAME, TXT, or SRV"
	 * @param array $options	array of key-value pairs for other optional values (see defaults array)
	 *
	 * @throws LinodeException
	 *
	 * @return number resource identifier
	 */
	public function create($domainid, $type, array $options = array())
	{
		$options['domainid'] = $domainid;
		$options['type'] = $type;

		$command = $this->prefix . '.create';
		$request_headers = array();
		$request_options['query'] = $this->processOptions($options);

		$data = $this->linode->get($command, $request_headers, $request_options);

		if (!array_key_exists('ResourceID', $data)) throw new LinodeException("Invalid data returned from {$command} - no ResourceID found");

		return $data['ResourceID'];
	}

	/**
	 * Create an MX record
	 *
	 * @param number $domainid		domainid of the domain to create the MX record for
	 * @param string $mail_server	name of the mail server
	 * @param number $priority		optional priority of this record
	 * @param string $subdomain		optional subdomain for the record
	 *
	 * @return number resourceid
	 */
	public function createMx($domainid, $mail_server, $priority = 0, $subdomain = "")
	{
		$options['target'] = $mail_server;
		if ($priority > 0) $options['priority'] = $priority;
		if (!empty($subdomain)) $options['name'] = $subdomain;

		return $this->create($domainid, "MX", $options);
	}

	/**
	 * Create an A record
	 *
	 * @param number $domainid		domainid of the domain to create the A record for
	 * @param string $hostname		name of the server
	 * @param string $ipaddress		address of the server
	 *
	 * @return number resourceid
	 */
	public function createA($domainid, $hostname, $ipaddress)
	{
		$options['name'] = $hostname;
		$options['target'] = $ipaddress;

		return $this->create($domainid, "A", $options);
	}

	/**
	 * Create an AAAA record
	 *
	 * @param number $domainid		domainid of the domain to create the A record for
	 * @param string $hostname		name of the server
	 * @param string $ipaddress		address of the server
	 *
	 * @return number resourceid
	 */
	public function createAAAA($domainid, $hostname, $ipaddress)
	{
		$options['name'] = $hostname;
		$options['target'] = $ipaddress;

		return $this->create($domainid, "AAAA", $options);
	}

	/**
	 * Create a CNAME record
	 *
	 * @param number $domainid		domainid of the domain to create the CNAME record for
	 * @param string $name			name to create record for
	 * @param string $target		target of the name alias
	 *
	 * @return number resourceid
	 */
	public function createCname($domainid, $name, $target)
	{
		$options['name'] = $name;
		$options['target'] = $target;

		return $this->create($domainid, "CNAME", $options);
	}

	/**
	 * Create a TXT record
	 *
	 * @param number $domainid		domainid of the domain to create the TXT record for
	 * @param string $name			name to create record for
	 * @param string $target		target of the name alias
	 *
	 * @return number resourceid
	 */
	public function createTxt($domainid, $name, $target)
	{
		$options['name'] = $name;
		$options['target'] = $target;

		return $this->create($domainid, "TXT", $options);
	}

	/**
	 * Create a NS record
	 *
	 * @param number $domainid		domainid of the domain to create the NS record for
	 * @param string $name_server	name of the name server
	 * @param string $subdomain		optional subdomain for the record
	 *
	 * @return number resourceid
	 */
	public function createNs($domainid, $name_server, $subdomain = "")
	{
		$options['target'] = $name_server;
		if (!empty($subdomain)) $options['name'] = $subdomain;

		return $this->create($domainid, "NS", $options);
	}

	/**
	 * Create an SRV record
	 *
	 * @param number $domainid		domainid of the domain to create the SRV record for
	 * @param string $service		name of the service
	 * @param string $target		target of the service
	 * @param string $protocol		optional protocol for this record
	 * @param number $priority		optional priority of this record
	 * @param number $weight		optional weight
	 * @param number $port			optional port
	 *
	 * @return number resourceid
	 */
	public function createSrv($domainid, $service, $target, $protocol = "", $priority = 0, $weight = 0, $port = 0)
	{
		$options['name'] = $service;
		$options['target'] = $target;
		if (!empty($protocol)) $options['protocol'] = $protocol;
		if ($priority > 0) $options['priority'] = $priority;
		if ($weight > 0) $options['weight'] = $weight;
		if ($port > 0) $options['port'] = $port;

		return $this->create($domainid, "SRV", $options);
	}

	/**
	 * domain.resource.update
	 *
	 * @param string $domainid		domainid to create a resource for
	 * @param string $resourceid	resourceid to update
	 * @param array $options		array of key-value pairs for other optional values (see defaults array)
	 *
	 * @throws LinodeException
	 *
	 * @return number resource identifier
	 */
	public function update($domainid, $resourceid, array $options = array())
	{
		$options['domainid'] = $domainid;
		$options['resourceid'] = $resourceid;

		$command = $this->prefix . '.update';
		$request_headers = array();
		$request_options['query'] = $this->processOptions($options);

		$data = $this->linode->get($command, $request_headers, $request_options);

		if (!array_key_exists('ResourceID', $data)) throw new LinodeException("Invalid data returned from {$command} - no ResourceID found");

		return $data['ResourceID'];
	}

	/**
	 * Update an MX record
	 *
	 * @param number $domainid		domainid of the domain to create the MX record for
	 * @param number $resourceid	resourceid to update
	 * @param string $mail_server	name of the mail server
	 * @param number $priority		optional priority of this record
	 * @param string $subdomain		optional subdomain for the record
	 * @param number $ttl			optional TTL value for this record
	 *
	 * @return number resourceid
	 */
	public function updateMx($domainid, $resourceid, $mail_server = "", $priority = 0, $subdomain = "", $ttl = 0)
	{
		if (!empty($mail_server)) $options['target'] = $mail_server;
		if ($priority > 0) $options['priority'] = $priority;
		if (!empty($subdomain)) $options['name'] = $subdomain;
		if ($ttl > 0) $options['ttl_sec'] = $ttl;

		return $this->update($domainid, $resourceid, $options);
	}

	/**
	 * Update an A record
	 *
	 * @param number $domainid		domainid of the domain to create the A record for
	 * @param number $resourceid	resourceid to update
	 * @param string $hostname		name of the server
	 * @param string $ipaddress		address of the server
	 * @param number $ttl			optional TTL value for this record
	 *
	 * @return number resourceid
	 */
	public function updateA($domainid, $resourceid, $hostname = "", $ipaddress = "", $ttl = 0)
	{
		if (!empty($hostname)) $options['name'] = $hostname;
		if (!empty($ipaddress)) $options['target'] = $ipaddress;
		if ($ttl > 0) $options['ttl_sec'] = $ttl;

		return $this->update($domainid, $resourceid, $options);
	}

	/**
	 * Update an AAAA record
	 *
	 * @param number $domainid		domainid of the domain to create the A record for
	 * @param number $resourceid	resourceid to update
	 * @param string $hostname		name of the server
	 * @param string $ipaddress		address of the server
	 * @param number $ttl			optional TTL value for this record
	 *
	 * @return number resourceid
	 */
	public function updateAAAA($domainid, $resourceid, $hostname = "", $ipaddress = "", $ttl = 0)
	{
		return $this->updateA($domainid, $resourceid, $hostname, $ipaddress, $ttl);
	}


	/**
	 * Update a CNAME record
	 *
	 * @param number $domainid		domainid of the domain to create the CNAME record for
	 * @param number $resourceid	resourceid to update
	 * @param string $name			name
	 * @param string $target		target of the name alias
	 * @param number $ttl			optional TTL value for this record
	 *
	 * @return number resourceid
	 */
	public function updateCname($domainid, $resourceid, $name = "", $target = "", $ttl = 0)
	{
		if (!empty($name)) $options['name'] = $name;
		if (!empty($target)) $options['target'] = $target;
		if ($ttl > 0) $options['ttl_sec'] = $ttl;

		return $this->update($domainid, $resourceid, $options);
	}

	/**
	 * Update a TXT record
	 *
	 * @param number $domainid		domainid of the domain to create the TXT record for
	 * @param number $resourceid	resourceid to update
	 * @param string $name			name
	 * @param string $target		target of the name alias
	 * @param number $ttl			optional TTL value for this record
	 *
	 * @return number resourceid
	 */
	public function updateTxt($domainid, $resourceid, $name = "", $target = "", $ttl = 0)
	{
		if (!empty($name)) $options['name'] = $name;
		if (!empty($target)) $options['target'] = $target;
		if ($ttl > 0) $options['ttl_sec'] = $ttl;

		return $this->update($domainid, $resourceid, $options);
	}

	/**
	 * Update a NS record
	 *
	 * @param number $domainid		domainid of the domain to create the NS record for
	 * @param number $resourceid	resourceid to update
	 * @param string $name_server	name of the name server
	 * @param string $subdomain		optional subdomain for the record
	 * @param number $ttl			optional TTL value for this record
	 *
	 * @return number resourceid
	 */
	public function updateNs($domainid, $resourceid, $name_server = "", $subdomain = "", $ttl = 0)
	{
		if (!empty($name_server)) $options['target'] = $name_server;
		if (!empty($subdomain)) $options['name'] = $subdomain;
		if ($ttl > 0) $options['ttl_sec'] = $ttl;

		return $this->update($domainid, $resourceid, $options);
	}

	/**
	 * Update an SRV record
	 *
	 * @param number $domainid		domainid of the domain to create the SRV record for
	 * @param number $resourceid	resourceid to update
	 * @param string $service		name of the service
	 * @param string $target		target of the service
	 * @param string $protocol		optional protocol for this record
	 * @param number $priority		optional priority of this record
	 * @param number $weight		optional weight
	 * @param number $port			optional port
	 *
	 * @return number resourceid
	 */
	public function updateSrv($domainid, $resourceid, $service, $target, $protocol = "", $priority = 0, $weight = 0, $port = 0, $ttl = 0)
	{
		$options['name'] = $service;
		$options['target'] = $target;
		if (!empty($protocol)) $options['protocol'] = $protocol;
		if ($priority > 0) $options['priority'] = $priority;
		if ($weight > 0) $options['weight'] = $weight;
		if ($port > 0) $options['port'] = $port;
		if ($ttl > 0) $options['ttl_sec'] = $ttl;

		return $this->update($domainid, $resourceid, $options);
	}

	/**
	 * domain.resource.delete
	 *
	 * @param number $domain_id		Linode domain ID resource belongs to
	 * @param number $resourceid 	Linode resource ID to delete for this domain
	 *
	 * @throws LinodeException
	 *
	 * @return number resource of the domain deleted
	 */
	public function delete($domainid, $resourceid)
	{
		$command = $this->prefix . '.delete';
		$request_headers = array();
		$request_options['query'] = array(
			"domainid" => intval($domainid),
			"resourceid" => intval($resourceid)
		);

		$data = $this->linode->get($command, $request_headers, $request_options);

		if (!array_key_exists('ResourceID', $data)) throw new LinodeException("Invalid data returned from {$command} - no ResourceID found");

		return $data['ResourceID'];
	}

	public function listResource($domainid, $resourceid = 0)
	{
		$command = $this->prefix . '.list';
		$request_headers = array();
		$request_options = array();

		$resourceid = intval($resourceid);

		$request_options['query'] = array("domainid" => intval($domainid));
		if ($resourceid > 0)
		{
			$request_options['query']['resourceid'] = $resourceid;
		}

		$data = $this->linode->get($command, $request_headers, $request_options);

		if ($resourceid > 0) return DomainResourceData::extractSingleResource($data, $resourceid);
		else return DomainResourceData::extractResources($data);
	}

	public function __call($func, $args)
	{
		switch ($func)
		{
			case 'list':
				if (!isset($args[0])) trigger_error("Missing argument 1 in call to ".__CLASS__."::$func()", E_USER_WARNING);
				else return $this->listResource($args[0], isset($args[1]) ? $args[1] : 0);
				break;
			default:
				trigger_error("Call to undefined method ".__CLASS__."::$func()", E_USER_ERROR);
				die ();
		}
	}
}

?>