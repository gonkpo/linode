<?php namespace Hampel\Linode\Command;

use Hampel\Linode\Service\LinodeService;

/**
 * Family abstract class - provides common functionality for API wrapper calls
 *
 */
abstract class Family
{
	/** @var string Prefix for commands */
	protected $prefix;

	/** @var LinodeService Our Linode service */
	protected $linode;

	/** @var array list of acceptable parameters for create and update calls */
	protected $parameters;

	/**
	 * Constructor
	 *
	 * @param LinodeService $linode
	 */
	public function __construct(LinodeService $linode)
	{
		$this->linode = $linode;
	}

	protected function processOptions($options)
	{
		$options_return = array();

		if (empty($this->parameters)) return $options_return;

		foreach ($this->parameters as $name)
		{
			if (isset($options[$name])) $options_return[$name] = $options[$name];
		}
		return $options_return;
	}
}

?>