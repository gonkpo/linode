<?php namespace Hampel\Linode\Command;

use Hampel\Linode\Service\LinodeException;

/**
 * User Linode API group
 *
 */
class User extends Family
{
	/** @var string Prefix for commands */
	protected $prefix = 'user';

	/**
	 * user.getapikey
	 *
	 * @param string username	username for linode manager login
	 * @param string password	password for linode manager login
	 *
	 * @throws LinodeException
	 *
	 * @return array api key
	 */
	public function getApiKey($username, $password)
	{
		$command = $this->prefix . '.getapikey';
		$request_headers = array();
		$request_options['query'] = array(
			'username' => $username,
			'password' => $password
		);

		$data = $this->linode->get($command, $request_headers, $request_options);

		if (!array_key_exists('API_KEY', $data)) throw new LinodeException("Invalid data returned from {$command} - no API_KEY found");

		return $data['API_KEY'];
	}
}

?>