<?php namespace Hampel\Linode\Response;

/**
 * Domain data
 *
 */
class DomainData extends Response
{

	public function getDomainId()
	{
		if (!empty($this->data) AND array_key_exists('domainid', $this->data)) return $this->data['domainid'];
		else return false;
	}

	public function getDomainName()
	{
		if (!empty($this->data) AND array_key_exists('domain', $this->data)) return $this->data['domain'];
		else return false;
	}

	public function getDomainType()
	{
		if (!empty($this->data) AND array_key_exists('type', $this->data)) return $this->data['type'];
		else return false;
	}

	public function getSoaEmail()
	{
		if (!empty($this->data) AND array_key_exists('soa_email', $this->data)) return $this->data['soa_email'];
		else return false;
	}

	/**
	 * Build an array of DomainData objects returned by a Linode API call
	 *
	 * @param array $data 		Array of domain arrays returned from decoded JSON data
	 *
	 * @return array of DomainData objects
	 */
	public static function extractDomains(array $data, $index = 'DOMAINID')
	{
		$domain_array = array();

		if (empty($data)) return $domain_array;

		foreach ($data as $domain_data)
		{
			if (array_key_exists($index, $domain_data)) // must use upper case array key, since we don't lowercase them until we "set" the data
			{
				$domain = new DomainData();
				$domain->set($domain_data);
				$domain_array[$domain_data[$index]] = $domain;
			}
		}

		return $domain_array;
	}

	/**
	 * Extract a single DomainData object returned by a Linode API call
	 *
	 * @param array $data 		Array of domain arrays returned from decoded JSON data
	 *
	 * @return array of DomainData objects
	 */
	public static function extractSingleDomain(array $data, $domainid)
	{
		$domains = self::extractDomains($data, 'DOMAIN');

		if (array_key_exists($domainid, $domains)) return $domains[$domainid];
		else return array();
	}

}

?>