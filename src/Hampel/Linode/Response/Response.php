<?php namespace Hampel\Linode\Response;

/**
 * Parent class for Linode responses - provides common functionality
 *
 */
abstract class Response
{
	/** @var array name-value pairs extracted from JSON data from Linode */
	protected $data = array();

	/**
	 * Constructor
	 *
	 * @param array $data name-value pairs
	 */
	public function __construct(array $data = array())
	{
		if (!empty($data))
		{
			$this->set($data);
		}
	}

	/**
	 * Set data to array
	 *
	 * @param array $data name-value pairs
	 */
	public function set(array $data)
	{
		if (!empty($data))
		{
			foreach ($data as $key => $value)
			{
				$this->data[strtolower($key)] = $value;
			}
		}
	}

	/**
	 * Get the name-value pair array
	 *
	 * @return array name-value pairs
	 */
	public function get()
	{
		return $this->data;
	}
}

?>