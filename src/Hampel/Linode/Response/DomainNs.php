<?php namespace Hampel\Linode\Response;
/**
 * 
 */

class DomainNs extends DomainResourceData
{
	public function getNameServer()
	{
		if (!empty($this->data) AND array_key_exists('target', $this->data)) return $this->data['target'];
		else return false;
	}

	public function getSubdomain()
	{
		if (!empty($this->data) AND array_key_exists('name', $this->data)) return $this->data['name'];
		else return false;
	}


}

?>
