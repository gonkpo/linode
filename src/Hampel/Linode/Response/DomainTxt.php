<?php namespace Hampel\Linode\Response;
/**
 * 
 */

class DomainTxt extends DomainResourceData
{
	public function getName()
	{
		if (!empty($this->data) AND array_key_exists('name', $this->data)) return $this->data['name'];
		else return false;
	}

	public function getValue()
	{
		if (!empty($this->data) AND array_key_exists('target', $this->data)) return $this->data['target'];
		else return false;
	}

}

?>
