<?php namespace Hampel\Linode\Response;
/**
 * 
 */

class DomainSrv extends DomainResourceData
{
	public function getName()
	{
		if (!empty($this->data) AND array_key_exists('name', $this->data)) return $this->data['name'];
		else return false;
	}

	public function getTarget()
	{
		if (!empty($this->data) AND array_key_exists('target', $this->data)) return $this->data['target'];
		else return false;
	}

	public function getPriority()
	{
		if (!empty($this->data) AND array_key_exists('priority', $this->data)) return $this->data['priority'];
		else return false;
	}

	public function getWeight()
	{
		if (!empty($this->data) AND array_key_exists('weight', $this->data)) return $this->data['weight'];
		else return false;
	}

	public function getPort()
	{
		if (!empty($this->data) AND array_key_exists('port', $this->data)) return $this->data['port'];
		else return false;
	}

	public function getProtocol()
	{
		if (!empty($this->data) AND array_key_exists('protocol', $this->data)) return $this->data['protocol'];
		else return false;
	}

}

?>
