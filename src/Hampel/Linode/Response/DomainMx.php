<?php namespace Hampel\Linode\Response;
/**
 * 
 */

class DomainMx extends DomainResourceData
{
	public function getSubdomain()
	{
		if (!empty($this->data) AND array_key_exists('name', $this->data)) return $this->data['name'];
		else return false;
	}

	public function getHostname()
	{
		if (!empty($this->data) AND array_key_exists('target', $this->data)) return $this->data['target'];
		else return false;
	}

	public function getPriority()
	{
		if (!empty($this->data) AND array_key_exists('priority', $this->data)) return $this->data['priority'];
		else return false;
	}
}

?>
