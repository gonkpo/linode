<?php namespace Hampel\Linode\Response;

/**
 * Domain Resource data
 *
 */
class DomainResourceData extends Response
{

	public function getDomainId()
	{
		if (!empty($this->data) AND array_key_exists('domainid', $this->data)) return $this->data['domainid'];
		else return false;
	}

	public function getResourceId()
	{
		if (!empty($this->data) AND array_key_exists('resourceid', $this->data)) return $this->data['resourceid'];
		else return false;
	}

	public function getType()
	{
		if (!empty($this->data) AND array_key_exists('type', $this->data)) return strtoupper($this->data['type']);
		else return false;
	}

	public function getTtl()
	{
		if (!empty($this->data) AND array_key_exists('ttl_sec', $this->data)) return $this->data['ttl_sec'];
		else return false;
	}

	/**
	 * Build an array of DomainResourceData objects returned by a Linode API call
	 *
	 * @param array $data 		Array of domain resource arrays returned from decoded JSON data
	 *
	 * @return array of DomainResourceData objects
	 */
	public static function extractResources(array $data)
	{
		$resource_array = array();

		$typeMap = array(
			'A' => 'A',
			'AAAA' => 'Aaaa',
			'CNAME' => 'Cname',
			'MX' => 'Mx',
			'NS' => 'Ns',
			'SRV' => 'Srv',
			'TXT' => 'Txt'
		);

		if (empty($data)) return $resource_array;

		foreach ($data as $resource_data)
		{
			$type = strtoupper($resource_data['TYPE']);
			if (!array_key_exists($type, $typeMap)) return array(); // bail now - bad data

			$typeName = '\Hampel\Linode\Response\Domain' . $typeMap[$type];

			$resource = new $typeName;
			$resource->set($resource_data);
			$resource_array[$resource->getResourceId()] = $resource;
		}

		return $resource_array;
	}

	/**
	 * Extract a single DomainResourceData object returned by a Linode API call
	 *
	 * @param array $data 		Array of domain resource arrays returned from decoded JSON data
	 *
	 * @return array of DomainResourceData objects
	 */
	public static function extractSingleResource(array $data, $resourceid)
	{
		$resources = self::extractResources($data);

		if (array_key_exists($resourceid, $resources)) return $resources[$resourceid];
		else return array();
	}

}

?>