<?php namespace Hampel\Linode\Response;
/**
 * 
 */

class DomainA extends DomainResourceData
{
	public function getHostname()
	{
		if (!empty($this->data) AND array_key_exists('name', $this->data)) return $this->data['name'];
		else return false;
	}

	public function getIp()
	{
		if (!empty($this->data) AND array_key_exists('target', $this->data)) return $this->data['target'];
		else return false;
	}
}

?>
