<?php namespace Hampel\Linode\Service;

use Hampel\Linode\Command\User;

/**
 * Provides configuration information for the Linode service
 *
 * You might want to create your own subclass to provide additional functionality for things like reading config from a database or from the file system
 *
 */
class LinodeConfig
{
	/** @var string API key either generated using Linode management console, or created via API call */
	protected $api_key;

	/**
	 * Set configuration
	 *
	 * @param string $api_key
	 */
	public function set($api_key)
	{
		$this->api_key = $api_key;
	}

	/**
	 * Get the API key
	 *
	 * @return string
	 */
	public function getApiKey()
	{
		return $this->api_key;
	}

	/**
	 * request a new API Key from Linode using Linode manager username and password
	 *
	 * @param string $username	Linode manager usernmae
	 * @param string $password	Linode manager password
	 *
	 * @return string API key
	 */
	public static function requestApiKey($username, $password)
	{
		$linode = LinodeService::build("");
		$user = new User($linode);

		return $user->getAPIKey($username, $password);
	}
}

?>