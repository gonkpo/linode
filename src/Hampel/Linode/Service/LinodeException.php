<?php namespace Hampel\Linode\Service;

use Hampel\Json\Json;
use Hampel\Json\JsonException;

/**
 * Custom Linode Exception which provides additional information for Linode errors
 *
 */
class LinodeException extends \Exception
{
	/** @var array extra information about Linode errors. Not currently used! */
	protected $linode_errors = array(
		0 => "OK",
		1 => "Bad request",
		2 => "No action was requested",
		3 => "The requested class does not exist",
		4 => "Authentication failed",
		5 => "Object not found",
		6 => "A required property is missing for this action",
		7 => "Property is invalid",
		8 => "A data validation error has occurred",
		9 => "Method not implemented",
		10 => "Too many batched requests",
		11 => "RequestArray isn't valid JSON or WDDX",
		12 => "Batch approaching timeout. Stopping here.",
		13 => "Permission denied",
		14 => "API rate limit exceeded",
		30 => "Charging the credit card failed",
		31 => "Credit card is expired",
		40 => "Limit of Linodes added per hour reached",
		41 => "Linode must have no disks before delete"
	);

	private function process_error_array(array $errors)
	{
		$error_strings = array();
		$error_string = "";

		if (!empty($errors) AND is_array($errors))
		{
			foreach ($errors as $error)
			{
				$error_strings[] = "{$error['ERRORCODE']}: {$error['ERRORMESSAGE']}";
			}

			$error_string = implode("; ", $error_strings);
		}

		return $error_string;
	}

	/**
	 * Custom constructor
	 *
	 * @param string $message		the message that corresponds to this error. Additional information may be appended to this message
	 * @param number $code			the numeric code associated with this error (if any)
	 * @param string $json_data		the full JSON returned by Linode API call
	 */
	public function __construct($message, $code = null, array $errors = array())
	{
		if (!is_null($code)) $message = "Linode API returned HTTP error {$message} (code: {$code})";

		if (!empty($errors))
		{
			if (isset($errors[0]['ERRORCODE'])) $code = $errors[0]['ERRORCODE']; // set our error code to the first of the codes returned by Linode
			$message .= " Errors: " . $this->process_error_array($errors);
		}

		parent::__construct($message, $code);
	}
}

?>