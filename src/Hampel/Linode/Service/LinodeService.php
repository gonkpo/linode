<?php namespace Hampel\Linode\Service;

use Guzzle\Http\Client;
use Hampel\Json\Json;

/**
 * The main service interface using Guzzle
 *
 */
class LinodeService
{
	/** var string base url for API calls */
	protected $base_url = 'https://api.linode.com/';

	/** @var LinodeConfig our config object */
	protected $config;

	/** @var Client our Guzzle HTTP Client object */
	protected $client;

	/** @var Request Guzzle Request object representing the last request sent via Guzzle */
	protected $last_request;

	/** @var Response Guzzle Response object representing the last response from Guzzle call to Linode API */
	protected $last_response;

	/**
	 * Constructor
	 *
	 * @param Client $client		Guzzle HTTP client
	 * @param LinodeConfig $config	Config object
	 */
	public function __construct(Client $client, LinodeConfig $config = null)
	{
		$this->client = $client;
		$this->config = $config;
	}

	/**
	 * Init - must be called after construction and before any other actions on this class
	 */
	public function init()
	{
		$this->client->setBaseUrl($this->base_url);
	}

	/**
	 * Build - do all the basic initialisation work automatically, using an API key
	 *
	 * @param string $api_key
	 * @return LinodeService a fully hydrated Linode Service, ready to run
	 */
	public static function build($api_key)
	{
		$config = new LinodeConfig();
		$config->set($api_key);
		$client = new Client();
		$linode = new LinodeService($client, $config);
		$linode->init();
		return $linode;
	}

	/**
	 * Make a GET call to the API via Guzzle
	 *
	 * @param string $command	Linode command to send
	 * @param array $request_headers	array of header information
	 * @param array $request_options	array of options, including API call parameters
	 *
	 * @throws LinodeException
	 *
	 * @return Response Guzzle Response object
	 */
	public function get($command, array $request_headers, array $request_options)
	{
		if (!isset($request_options['exceptions']))	$request_options['exceptions'] = false;

		if (($api_key = $this->getApiKey()) !== false) $request_options['query']['api_key'] = $api_key;
		$request_options['query']['api_action'] = $command;

		$request = $this->client->get("", $request_headers, $request_options);
		$this->last_request = $request;
		$response = $this->client->send($request);

		if (!$response->isSuccessful()) throw new LinodeException($response->getReasonPhrase(), $response->getStatusCode(), $response->getBody());
		$this->last_response = $response;

		$response_json = $response->getBody(true);
		if (empty($response_json)) throw new LinodeException("Empty body received from {$command}");

		$decoded_data = Json::decode($response_json, true);

		if (!array_key_exists('ERRORARRAY', $decoded_data)) throw new LinodeException("Invalid data received from {$command} - no ERRORARRAY");
		if (!array_key_exists('DATA', $decoded_data)) throw new LinodeException("Invalid data received from {$command} - no DATA");
		if (!empty($decoded_data['ERRORARRAY'])) throw new LinodeException("Error from Linode API call {$command}", null, $decoded_data['ERRORARRAY']);

		return $decoded_data['DATA'];
	}

	/**
	 * Return the status code from the last API call made
	 *
	 * @return number status code
	 */
	public function getLastStatusCode()
	{
		return $this->last_response->getStatusCode();
	}

	/**
	 * Return the request object from the last API call made
	 *
	 * @return Request Guzzle Request object
	 */
	public function getLastRequest()
	{
		return $this->last_request;
	}

	public function getLastQuery()
	{
		return $this->getLastRequest()->getQuery()->__toString();
	}

	/**
	 * Return the response object from the last API call made
	 *
	 * @return Response Guzzle Reponse object
	 */
	public function getLastResponse()
	{
		return $this->last_response;
	}

	public function getApiKey()
	{
		if (is_null($this->config)) return false;
		else return $this->config->getApiKey();
	}
}

?>