CHANGELOG
=========

1.0.0 (2013-08-28)
------------------

* added new function DomainResourceData::getResourceType()
* added new functions DomainResource::createTxt() and DomainResource::updateTxt()
* added new function DomainData::getDomainType()
* refactored some functions not using camel-case
* DomainResourceData::extractSingleResource()
* added resourceid to array key in DomainResourceData::extractResources()
* modified DomainResourceData::extractSingleResource() to take second parameter, being the resourceid
* added function DomainResourceData::getDomainId()
* added helper classes for domain resource types
* Added function DomainResource::createAAAA(), DomainResource::updateAAAA(), added TTL parameter to update functions
* added helpers for creating and updating NS and SRV records
* fixed bug in DomainResourceData::extractResources()

0.3.0 (2013-07-14)
------------------

* implemented domain.resource.* calls
* fixed some problems with the domain.* calls
* implemented aliases for echo and list calls to get around PHP reserved word limitations
* added some helper functions createA, createMX, createCNAME, updateA, updateMX, updateCNAME
* changed response data storage to use lower case keys
* changed DomainData::extractDomains to return domainids as array keys
* added DomainData::getDomainName function

0.2.1 (2013-07-03)
------------------

* changed how option handling is managed

0.2.0 (2013-07-03)
------------------

* complete rewrite as a Composer package using Guzzle
* implemented domain.*, test.echo and user.getapikey calls

0.1.0 (2011-12-06)
------------------

* initial release
* implemented domain.* and domain.resource.* calls, plus test.echo