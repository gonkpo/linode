<?php namespace Hampel\Linode\Command;

use Hampel\Linode\Service\LinodeService;
use Guzzle\Service\Client;
use Guzzle\Tests\GuzzleTestCase;

class DomainTest extends GuzzleTestCase
{
	public function setUp()
	{
		$this->setMockBasePath(dirname(__FILE__) . DIRECTORY_SEPARATOR . "mock");
	}

	public function testMissingEmail()
	{
		$client = new Client();
		$this->setMockResponse($client, 'domain_create_8.json');
		$this->setExpectedException('Hampel\Linode\Service\LinodeException', 'Error from Linode API call domain.create Errors: 8: SOA_Email is required when Type=master');

		$linode = new LinodeService($client);
		$linode->init();
		$domain = new Domain($linode);
		$domain_id = $domain->create("mock-domain.com");
	}

	public function testCreate()
	{
		$client = new Client();
		$this->setMockResponse($client, 'domain_create.json');

		$linode = new LinodeService($client);
		$linode->init();
		$domain = new Domain($linode);
		$response = $domain->create("mock-domain.com", "master", array("soa_email" => "mock@mock-domain.com"));

		$this->assertEquals('domain=mock-domain.com&type=master&soa_email=mock%40mock-domain.com&api_action=domain.create', $linode->getLastQuery());
		$this->assertEquals(200, $linode->getLastStatusCode());
		$this->assertEquals(12345, $response);
	}

	public function testUpdate()
	{
		$client = new Client();
		$this->setMockResponse($client, 'domain_update.json');

		$linode = new LinodeService($client);
		$linode->init();
		$domain = new Domain($linode);
		$response = $domain->update(12345);

		$this->assertEquals('domainid=12345&api_action=domain.update', $linode->getLastQuery());
		$this->assertEquals(200, $linode->getLastStatusCode());
		$this->assertEquals(12345, $response);
	}

	public function testDelete()
	{
		$client = new Client();
		$this->setMockResponse($client, 'domain_delete.json');

		$linode = new LinodeService($client);
		$linode->init();
		$domain = new Domain($linode);
		$response = $domain->delete(12345);

		$this->assertEquals('domainid=12345&api_action=domain.delete', $linode->getLastQuery());
		$this->assertEquals(200, $linode->getLastStatusCode());
		$this->assertEquals(12345, $response);
	}

	public function testListSingle()
	{
		$client = new Client();
		$this->setMockResponse($client, 'domain_list_single.json');

		$linode = new LinodeService($client);
		$linode->init();
		$domain = new Domain($linode);
		$response = $domain->list(12345);

		$this->assertEquals('domainid=12345&api_action=domain.list', $linode->getLastQuery());
		$this->assertEquals(200, $linode->getLastStatusCode());
		$this->assertEquals(12345, $response->getDomainId());
	}

	public function testList()
	{
		$client = new Client();
		$this->setMockResponse($client, 'domain_list_multiple.json');

		$linode = new LinodeService($client);
		$linode->init();
		$domain = new Domain($linode);
		$response = $domain->list();

		$this->assertEquals('api_action=domain.list', $linode->getLastQuery());
		$this->assertEquals(200, $linode->getLastStatusCode());
		$this->assertEquals(3, count($response));

		$domainids = array_keys($response);

		$this->assertEquals(12345, array_shift($domainids));
		$this->assertEquals(12346, array_shift($domainids));
		$this->assertEquals(12347, array_shift($domainids));

		$this->assertEquals('mock-domain.com', array_shift($response)->getDomainName());
		$this->assertEquals('mock-domain2.com', array_shift($response)->getDomainName());
		$this->assertEquals('mock-domain3.com', array_shift($response)->getDomainName());
	}
}

?>