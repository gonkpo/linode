<?php namespace Hampel\Linode\Command;

use Hampel\Linode\Service\LinodeService;
use Guzzle\Service\Client;
use Guzzle\Tests\GuzzleTestCase;

class DomainResourceTest extends GuzzleTestCase
{
	public function setUp()
	{
		$this->setMockBasePath(dirname(__FILE__) . DIRECTORY_SEPARATOR . "mock");
	}

	public function testCreate()
	{
		$client = new Client();
		$this->setMockResponse($client, 'domain_resource_create.json');

		$linode = new LinodeService($client);
		$linode->init();
		$resource = new DomainResource($linode);
		$response = $resource->create(12345, "A", array("target" => "10.0.0.1"));

		$this->assertEquals('domainid=12345&type=A&target=10.0.0.1&api_action=domain.resource.create', $linode->getLastQuery());
		$this->assertEquals(200, $linode->getLastStatusCode());
		$this->assertEquals(54321, $response);
	}

	public function testCreateMx()
	{
		$client = new Client();
		$this->setMockResponse($client, 'domain_resource_create.json');

		$linode = new LinodeService($client);
		$linode->init();
		$resource = new DomainResource($linode);
		$response = $resource->createMx(12345, "mail.mock-domain.com");

		$this->assertEquals('domainid=12345&type=MX&target=mail.mock-domain.com&api_action=domain.resource.create', $linode->getLastQuery());
		$this->assertEquals(200, $linode->getLastStatusCode());
		$this->assertEquals(54321, $response);
	}

	public function testCreateA()
	{
		$client = new Client();
		$this->setMockResponse($client, 'domain_resource_create.json');

		$linode = new LinodeService($client);
		$linode->init();
		$resource = new DomainResource($linode);
		$response = $resource->createA(12345, "mock-domain.com", "10.0.0.1");

		$this->assertEquals('domainid=12345&type=A&name=mock-domain.com&target=10.0.0.1&api_action=domain.resource.create', $linode->getLastQuery());
		$this->assertEquals(200, $linode->getLastStatusCode());
		$this->assertEquals(54321, $response);
	}

	public function testCreateCname()
	{
		$client = new Client();
		$this->setMockResponse($client, 'domain_resource_create.json');

		$linode = new LinodeService($client);
		$linode->init();
		$resource = new DomainResource($linode);
		$response = $resource->createCname(12345, "mock", "mock-domain.com");

		$this->assertEquals('domainid=12345&type=CNAME&name=mock&target=mock-domain.com&api_action=domain.resource.create', $linode->getLastQuery());
		$this->assertEquals(200, $linode->getLastStatusCode());
		$this->assertEquals(54321, $response);
	}

	public function testUpdate()
	{
		$client = new Client();
		$this->setMockResponse($client, 'domain_resource_update.json');

		$linode = new LinodeService($client);
		$linode->init();
		$resource = new DomainResource($linode);
		$response = $resource->update(12345, 54321, array("target" => "10.0.0.1"));

		$this->assertEquals('domainid=12345&resourceid=54321&target=10.0.0.1&api_action=domain.resource.update', $linode->getLastQuery());
		$this->assertEquals(200, $linode->getLastStatusCode());
		$this->assertEquals(54321, $response);
	}

	public function testUpdateMx()
	{
		$client = new Client();
		$this->setMockResponse($client, 'domain_resource_update.json');

		$linode = new LinodeService($client);
		$linode->init();
		$resource = new DomainResource($linode);
		$response = $resource->updateMx(12345, 54321, "mail.mock-domain.com");

		$this->assertEquals('domainid=12345&resourceid=54321&target=mail.mock-domain.com&api_action=domain.resource.update', $linode->getLastQuery());
		$this->assertEquals(200, $linode->getLastStatusCode());
		$this->assertEquals(54321, $response);
	}

	public function testUpdateA()
	{
		$client = new Client();
		$this->setMockResponse($client, 'domain_resource_update.json');

		$linode = new LinodeService($client);
		$linode->init();
		$resource = new DomainResource($linode);
		$response = $resource->updateA(12345, 54321, "mail", "10.0.0.1");

		$this->assertEquals('domainid=12345&resourceid=54321&name=mail&target=10.0.0.1&api_action=domain.resource.update', $linode->getLastQuery());
		$this->assertEquals(200, $linode->getLastStatusCode());
		$this->assertEquals(54321, $response);
	}

	public function testUpdateCname()
	{
		$client = new Client();
		$this->setMockResponse($client, 'domain_resource_update.json');

		$linode = new LinodeService($client);
		$linode->init();
		$resource = new DomainResource($linode);
		$response = $resource->updateCname(12345, 54321, "mail", "mail.mock-domain.com");

		$this->assertEquals('domainid=12345&resourceid=54321&name=mail&target=mail.mock-domain.com&api_action=domain.resource.update', $linode->getLastQuery());
		$this->assertEquals(200, $linode->getLastStatusCode());
		$this->assertEquals(54321, $response);
	}

	public function testDelete()
	{
		$client = new Client();
		$this->setMockResponse($client, 'domain_resource_delete.json');

		$linode = new LinodeService($client);
		$linode->init();
		$resource = new DomainResource($linode);
		$response = $resource->delete(12345, 54321);

		$this->assertEquals('domainid=12345&resourceid=54321&api_action=domain.resource.delete', $linode->getLastQuery());
		$this->assertEquals(200, $linode->getLastStatusCode());
		$this->assertEquals(54321, $response);
	}

	public function testListSingle()
	{
		$client = new Client();
		$this->setMockResponse($client, 'domain_resource_list_single.json');

		$linode = new LinodeService($client);
		$linode->init();
		$resource = new DomainResource($linode);
		$response = $resource->list(12345, 54321);

		$this->assertEquals('domainid=12345&resourceid=54321&api_action=domain.resource.list', $linode->getLastQuery());
		$this->assertEquals(200, $linode->getLastStatusCode());
		$this->assertEquals(54321, $response->getResourceId());
		$this->assertEquals('A', $response->getType());
	}

	public function testList()
	{
		$client = new Client();
		$this->setMockResponse($client, 'domain_resource_list_multiple.json');

		$linode = new LinodeService($client);
		$linode->init();
		$resource = new DomainResource($linode);
		$response = $resource->list(12345);

		$this->assertEquals('domainid=12345&api_action=domain.resource.list', $linode->getLastQuery());
		$this->assertEquals(200, $linode->getLastStatusCode());
		$this->assertEquals(2, count($response));
		$this->assertArrayHasKey(54321, $response);
		$this->assertEquals(54321, $response[54321]->getResourceId());
		$this->assertEquals('A', $response[54321]->getType());
		$this->assertArrayHasKey(54322, $response);
		$this->assertEquals(54322, $response[54322]->getResourceId());
		$this->assertEquals('A', $response[54322]->getType());
	}
}

?>