<?php namespace Hampel\Linode\Service;

use Guzzle\Tests\GuzzleTestCase;
use Guzzle\Service\Client;

class LinodeServiceTest extends GuzzleTestCase
{
	public function setUp()
	{
		$this->setMockBasePath(dirname(__FILE__) . DIRECTORY_SEPARATOR . "mock");
	}

	public function testAuthFail()
	{
		$client = new Client();
		$this->setMockResponse($client, 'auth_failed_4.json');
		$this->setExpectedException('Hampel\Linode\Service\LinodeException', 'Error from Linode API call test.echo Errors: 4: Authentication failed');

		$linode = new LinodeService($client);
		$linode->init();
		$response = $linode->get('test.echo', array(), array());
	}

	public function testEcho()
	{
		$client = new Client();
		$this->setMockResponse($client, 'test_echo.json');

		$linode = new LinodeService($client);
		$linode->init();
		$options = array('query' => array("foo" => "bar"));
		$response = $linode->get('test.echo', array(), $options);

		$this->assertEquals(200, $linode->getLastStatusCode());
		$this->assertArrayHasKey('foo', $response);
		$this->assertEquals('bar', $response['foo']);
	}
}

?>