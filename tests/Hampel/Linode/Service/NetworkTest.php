<?php namespace Hampel\Linode\Service;

use Hampel\Linode\Command\Test;

class NetworkTest extends \PHPUnit_Framework_TestCase
{
	protected $linode;

	public function setUp()
	{
		date_default_timezone_set('UTC');
		$this->linode = LinodeService::build(API_KEY);
	}

	/**
	 * @group network
	 */
	public function testEcho()
	{
		$options = array('query' => array("foo" => "bar"));
		$response = $this->linode->get("test.echo", array(), $options);

		$this->assertEquals(200, $this->linode->getLastStatusCode());
		$this->assertArrayHasKey('foo', $response);
		$this->assertEquals('bar', $response['foo']);
	}

	/**
	 * @group network
	 */
	public function testEchoTest()
	{
		$test = new Test($this->linode);
		$response = $test->echoTest(array("foo" => "bar"));
		$status_code = $this->linode->getLastStatusCode();

		$this->assertEquals(200, $this->linode->getLastStatusCode());
		$this->assertArrayHasKey('foo', $response);
		$this->assertEquals('bar', $response['foo']);
	}
}

?>